package com.bnppf.employee.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bnppf.employee.api.domain.Employee;
import com.bnppf.employee.api.exception.RecordNotFoundException;
import com.bnppf.employee.api.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository repository;

	public Employee create(Employee employee) {
		employee.getDepartments().forEach(
				department -> department.setEmployee(employee));
		return repository.save(employee);
	}

	public Employee fetchByEmployeeId(Integer employeeId) throws RecordNotFoundException {
		Optional<Employee> mayBeEmployee = repository.findById(employeeId);
		if(!mayBeEmployee.isPresent())
			throw new RecordNotFoundException("Employee record not found");
		return mayBeEmployee.get();
	}

}
