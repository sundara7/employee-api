package com.bnppf.employee.api.service;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.bnppf.employee.api.BaseTest;
import com.bnppf.employee.api.domain.Employee;
import com.bnppf.employee.api.exception.RecordNotFoundException;
import com.bnppf.employee.api.repository.EmployeeRepository;

@SpringBootTest
public class EmployeeServiceTest extends BaseTest {

	@Mock
	private EmployeeRepository repository;

	@InjectMocks
	private EmployeeService service;

	@Test
	public void serviceShouldCreateEmployeeDataWhenCreateServiceIsCalled()
			throws Exception {
		Employee employee = getEmployee();
		Mockito.when(repository.save(employee)).thenReturn(employee);

		Employee createdEmployee = service.create(employee);

		Assertions.assertEquals(employee.getId(), createdEmployee.getId());
		Assertions.assertEquals(employee.getName(), createdEmployee.getName());
		Assertions.assertEquals(employee.getDateOfBirth(),
				createdEmployee.getDateOfBirth());
		Assertions.assertEquals(employee.getAddress(),
				createdEmployee.getAddress());
		Assertions.assertEquals(employee.getDepartments().size(),
				createdEmployee.getDepartments().size());
		Assertions.assertEquals(employee.getDepartments().stream().findFirst()
				.get().getId(), createdEmployee.getDepartments().stream()
				.findFirst().get().getId());
		Assertions.assertEquals(employee.getDepartments().stream().findFirst()
				.get().getName(), createdEmployee.getDepartments().stream()
				.findFirst().get().getName());
	}

	@Test
	public void shouldReturnEmployeeDataWhenPassingEmployeeId()
			throws Exception {
		Employee employee = getEmployee();
		Mockito.when(repository.findById(1)).thenReturn(Optional.of(employee));

		Employee retrievedEmployee = service.fetchByEmployeeId(1);

		Assertions.assertEquals(employee.getId(), retrievedEmployee.getId());
		Assertions
				.assertEquals(employee.getName(), retrievedEmployee.getName());
		Assertions.assertEquals(employee.getDateOfBirth(),
				retrievedEmployee.getDateOfBirth());
		Assertions.assertEquals(employee.getAddress(),
				retrievedEmployee.getAddress());
		Assertions.assertEquals(employee.getDepartments().size(), employee
				.getDepartments().size());
		Assertions.assertEquals(employee.getDepartments().stream().findFirst()
				.get().getId(), retrievedEmployee.getDepartments().stream()
				.findFirst().get().getId());
		Assertions.assertEquals(employee.getDepartments().stream().findFirst()
				.get().getName(), retrievedEmployee.getDepartments().stream()
				.findFirst().get().getName());
	}

	@Test
	public void shouldThrowsRecordNotFoundExceptionWhenEmployeeDataNotFound()
			throws Exception {
		RecordNotFoundException exception = Assertions.assertThrows(
				RecordNotFoundException.class, () -> {
					service.fetchByEmployeeId(1);
				});

		Assertions.assertEquals("Employee record not found",
				exception.getMessage());
	}
}
