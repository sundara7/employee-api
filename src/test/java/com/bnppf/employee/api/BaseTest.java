package com.bnppf.employee.api;

import java.util.ArrayList;
import java.util.List;

import com.bnppf.employee.api.domain.Department;
import com.bnppf.employee.api.domain.Employee;

public class BaseTest {

	private static final int ID_1 = 1;

	public Employee getEmployee() {
		Employee employee = new Employee();
		employee.setId(ID_1);
		employee.setName("Employee1");
		employee.setDateOfBirth("07-07-1990");
		employee.setAddress("22 Fairylane Circle, Dearborn, Michigan");

		List<Department> departments = new ArrayList<Department>();
		Department department = new Department();
		department.setId(ID_1);
		department.setName("department1");
		departments.add(department);
		employee.setDepartments(departments);
		return employee;
	}
}
